install-transmission
====================

Transmission install script (CentOS). (updated for transmision v 2.83)

**Run the following commands to install on CentOS 6:**

```
wget https://github.com/elijahpaul/install-transmission/raw/master/install-transmission.sh

chmod u+x install-transmission.sh

./install-transmission.sh
```
ref: [http://elijahpaul.co.uk/script-install-for-transmission-2-83-seedbox-on-centos-6-5/](http://elijahpaul.co.uk/script-install-for-transmission-2-83-seedbox-on-centos-6-5/)

source: [http://transmissionseedbox.blogspot.de/2012/01/creating-seedbox-in-centos-6.html](http://transmissionseedbox.blogspot.de/2012/01/creating-seedbox-in-centos-6.html)
